##### My OpenPGP Master Key FingerPrint & Sub-Key IDs

```
pub   rsa4096/D7249E14EB59CD41 2019-04-09 [SC]
      9B4C 365E D2A3 7DD8 52C8 22A2 D724 9E14 EB59 CD41
uid   Max Wolf Martin <mwm@mwm.is>
sub   rsa4096/20D152C8756EC900 2019-04-09 [S] [expires: 2029-04-06]
sub   rsa4096/5BE315419838B74D 2019-04-09 [E] [expires: 2029-04-06]
sub   rsa4096/70D9E76AA47BB6A4 2019-04-09 [A] [expires: 2029-04-06]
```

